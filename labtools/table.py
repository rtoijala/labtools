#!/usr/bin/env python3

class Cell:
    """A cell for the Table class"""
    def __init__(self, data):
        self.data = str(data)

class Table:
    """A table for data used for converting to LaTeX."""
    def __init__(self):
        self._number_rows = 0
        self._number_columns = 0
        self._data = []
        self._headrow_length = 0
        self._headrow = []
        self._headcolumn_length = 0
        self._headcolumn = []
        self._constantrows = []
        self._hrules = []
        self._vrules = []

    def add_column(self, data, header=False):
        """Adds a column to the right of the table.
        All columns will be of the length of the longest column.
        """
        # add new rows and pad existing columns to new length if necessary
        while len(data) > len(self._data):
            self._data.append([])
            for column_number in range(self._number_columns):
                self._data[len(self._data) - 1].append(Cell(''))
        # add new data
        for row_number in range(len(data)):
            self._data[row_number].append(Cell(data[row_number]))
        # pad new column if necessary
        if len(data) < self._number_rows:
            for row_number in range(self._number_rows - len(data)):
                self._data[row_number + len(data)].append(Cell(''))
        # add header if wanted
        column_number = len(self._data[0]) - 1
        if header:
            if self._headrow_length > column_number and self._headrow[column_number].data != '':
                raise RuntimeError('A headrow cell already correspponds to the column.')
            while len(self._headrow) <= column_number:
                self._headrow.append(Cell(''))
            self._headrow[column_number] = Cell(header)
        self._update_headrow_size()
        self._update_data_sizes()

    def add_row(self, data, header=False):
        """Adds a row to the bottom of the table.
        All rows will be of the length of the longest row.
        """
        # pad existing rows to new length if necessary
        for row in self._data:
            while len(row) < len(data):
                row.append(Cell(''))
        self._data.append([])
        row_number = len(self._data) - 1
        # add new data
        for item in data:
            self._data[row_number].append(Cell(item))
        # pad new row if necessary
        while len(self._data[row_number]) < self._number_columns:
            self._data[row_number].append(Cell(''))
        # add header if wanted
        row_number = len(self._data) - 1
        if header:
            if self._headcolumn_length > row_number and self._headcolumn[row_number].data != '':
                raise RuntimeError('A headcolumn cell already corresponds to the row.')
            while len(self._headcolumn) <= row_number:
                self._headcolumn.append(Cell(''))
            self._headcolumn[row_number] = Cell(header)
        self._update_headcolumn_size()
        self._update_data_sizes()

    def add_headcolumn(self, headers, start_position=0):
        """Adds a header column to the left of the table."""
        if len(self._headcolumn) != 0:
            raise RuntimeError('A header column has already been added.')
        # add empty cells before headers if wanted
        for i in range(start_position):
            self._headcolumn.append(Cell(''))
        # add new headers
        for header in headers:
            self._headcolumn.append(Cell(header))
        self._update_headcolumn_size()

    def add_headrow(self, headers, start_position=0):
        """Adds a header row to the top of the table."""
        if len(self._headrow) != 0:
            raise RuntimeError('A header row has already been added.')
        # add empty cells before headers if wanted
        for i in range(start_position):
            self._headrow.append(Cell(''))
        # add new headers
        for header in headers:
            self._headrow.append(Cell(header))
        self._update_headrow_size()

    def add_constantrow(self, *args):
        """Add a row of constants to the top of the table.
        Constants are 2-tuples consisting of the latex representation of the constant's name and the value.
        """
        self._constantrows.append([])
        row_number = len(self._constantrows) - 1
        for constant in args:
            self._constantrows[row_number].append(constant)

    def add_hrule(self, row_number, rules=1):
        """Adds a horizontal rule to the table.

        The rule is added after the specified row.
        Rows are zero-indexed from the first row of data.
        Multiple rules may be added with the rules parameter.
        Rules before the first row of data and after the last row of data are not supported.
        """
        if type(row_number) != int:
            raise TypeError('The row number must be an integer.')
        if type(rules) != int:
            raise TypeError('The number of rules must be an integer.')
        if row_number < 0 or row_number > self._number_rows - 2:
            raise ValueError('The row number must be between 0 and the number of rows minus two.')
        if rules < 1:
            raise ValueError('The number of rules must be greater than 0.')
        self._hrules.append((row_number, rules))

    def add_vrule(self, column_number, rules=1):
        """Adds a vertical rule to the table.

        The rule is added after the specified column.
        Columns are zero-indexed.
        Multiple rules may be added with the rules parameter.
        Rules before the first column and after the last column are not supported.
        """
        if type(column_number) != int:
            raise TypeError('The column number must be an integer.')
        if type(rules) != int:
            raise TypeError('The number of rules must be an integer.')
        if column_number < 0 or column_number > self._number_rows - 2:
            raise ValueError('The row number must be between 0 and the number of rows minus two.')
        if rules < 1:
            raise ValueError('The number of rules must be greater than 0.')
        self._vrules.append((column_number, rules))

    def latex(self, header = 'S', toprule=True, headrule=True, vheadrule=False, bottomrule=True):
        """A method for creating a LaTeX table.

        Creates a LaTeX table from this Table instance.
        """

        self._expand_data_heads()
        self._hrules.sort(key=lambda hrule: hrule[0])
        self._vrules.sort(key=lambda vrule: vrule[0])

        # begin table
        output = '\\begin{tabular}'
        # column types
        thead = ''
        for column_number in range(self._number_columns):
            thead = thead + header
            # vrules
            while len(self._vrules) != 0 and self._vrules[0][0] == column_number and column_number != self._number_columns - 1:
                thead = thead + '|' * self._vrules[0][1]
                self._vrules = self._vrules[1:]
        # header column type & rule
        if len(self._headcolumn) != 0:
            if vheadrule:
                thead = '|' + thead
            thead = header + thead
        output = output + '{' + thead + '}\n'
        # toprule if wanted
        if toprule:
            output = output + '\t\\toprule\n'
        # header row
        header_row = ''
        if self._headrow_length != 0:
            header_row = header_row + '\t'
            # empty cell over header column
            if self._headcolumn_length != 0:
                header_row = header_row + ''.ljust(self._headcolumn_str_length()) + ' & '
            # header row
            for column_number in range(self._headrow_length):
                header_row = header_row + self._headrow[column_number].data.ljust(self._column_str_length(column_number))
                if column_number < self._headrow_length - 1:
                    header_row = header_row + ' & '
            header_row = header_row + ' \\\\\n'
        output = output + header_row
        # headrule if wanted
        if headrule and len(self._headrow) != 0:
            output = output + '\t\\midrule\n'
        # header column and data
        tabular = ''
        for row_number in range(self._number_rows):
            tabular = tabular + '\t'
            # header column cell
            if self._headcolumn_length != 0:
                tabular = tabular + self._headcolumn[row_number].data.ljust(self._headcolumn_str_length()) + ' & '
            # data cells
            for column_number in range(self._number_columns):
                tabular = tabular + self._data[row_number][column_number].data.ljust(self._column_str_length(column_number))
                if column_number < self._number_columns - 1:
                    tabular = tabular + ' & '
            tabular = tabular + ' \\\\\n'
            # hrules
            while len(self._hrules) != 0 and self._hrules[0][0] == row_number:
                tabular = tabular + '\t\\midrule\n' * self._hrules[0][1]
                self._hrules = self._hrules[1:]
        output = output + tabular
        # bottomrule if wanted
        if bottomrule:
            output = output + '\t\\bottomrule\n'
        # end tabular
        output = output + '\\end{tabular}\n'
        # constant rows
        output = output + '\\begin{center}\n'
        for row_number in range(len(self._constantrows)):
            output = output + '\t' + '\\quad'.join(['$' + x[0] + ' = ' + x[1] + '$' for x in self._constantrows[row_number]])
            if row_number < len(self._constantrows) - 1:
                output = output + '\\\\'
            output = output + '\n'
        output = output + '\\end{center}\n'
        return output

    def _headcolumn_str_length(self):
        return max(len(data) for data in map(lambda x: x.data, self._headcolumn))

    def _column_str_length(self, column_number):
        if self._headrow_length == 0:
            headrow_length = 0
        else:
            headrow_length = len(self._headrow[column_number].data)
        if self._number_columns == 0:
            data_length = 0
        else:
            data_length = max(len(row[column_number].data) for row in self._data)
        return max(headrow_length, data_length)

    def _update_data_sizes(self):
        self._number_rows = len(self._data)
        if len(self._data) == 0:
            self._number_columns = 0
        else:
            self._number_columns = len(self._data[0])

    def _update_headcolumn_size(self):
        self._headcolumn_length = len(self._headcolumn)

    def _update_headrow_size(self):
        self._headrow_length = len(self._headrow)

    def _expand_data_heads(self):
        while self._headcolumn_length != 0 and self._number_rows > self._headcolumn_length:
            self._headcolumn.append(Cell(''))
            self._update_headcolumn_size()
        while self._headcolumn_length != 0 and self._number_rows < self._headcolumn_length:
            self.add_row([])
            self._update_data_sizes()
        while self._headrow_length != 0 and self._number_columns > self._headrow_length:
            self._headrow.append(Cell(''))
            self._update_headrow_size()
        while self._headrow_length != 0 and self._number_columns < self._headrow_length:
            self.add_column([])
            self._update_data_sizes()

def silabel(variable, unit, factor=False):
    """Returns a siunitx label of the form variable / (unit * factor)."""
    ret = r'\silabel{' + str(variable) + r'}'
    if factor:
        ret = ret + r'[' + str(factor) + r']'
    ret = ret + r'{' + str(unit) + r'}'
    return ret
