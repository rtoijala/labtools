import os
import os.path
import math
from decimal import Decimal

import numpy
import numpy.linalg
import scipy
import scipy.stats
import scipy.constants
import uncertainties
from uncertainties import ufloat, ufloat_fromstr
from uncertainties.unumpy import nominal_values, std_devs, uarray

def heaviside(x):
    if x > 0:
        return x
    else:
        return 0

def round_figures(value, figures):
    d = Decimal(value)
    if d == 0:
        return '0'
    d = round(d, figures - int(math.floor(math.log10(abs(d)))) - 1)
    return "{:f}".format(d)

def round_places(value, places):
    d = Decimal(value)
    if d == 0:
        return '0'
    d = round(d, places)
    return "{:f}".format(d)

# rounds the given value + error to at most 15 units of error
# and returns the result as two strings
def round_error(value, error=None):
    # if error is None, value if a ufloat
    if error == None:
        error = value.std_dev
        value = value.nominal_value
    # error cannot be negative
    if error < 0:
        raise RuntimeError('The error must be positive or zero. It was ' + str(error) + '.')
    if error == 0:
        return ((str(value), '0'))

    if value != 0:
        figures_value = int(math.floor(math.log10(abs(value))))
        while value * 10**(-figures_value) % 1 != 0:
            figures_value -= 1
    else:
        figures_value = 0
    figures_error = int(math.floor(math.log10(error))) - 1
    max_figures = figures_error
    # error normalized to two significant digits before comma
    e_error = numpy.round(error * 10**(-figures_error), 0)
    # drop one digit if the error is too large or error only has one significant digit and value has less digits than error
    if e_error > 15 or (error * 10**(-figures_error) % 10 == 0 and figures_value >= figures_error):
        max_figures += 1
        e_error = int(numpy.round(e_error, -1))
    # correctly rounded error and value
    r_error = e_error * 10**figures_error
    r_value = numpy.round(value, -max_figures)
    if max_figures < 0:
        return((('{:.' + str(-max_figures) + 'f}').format(r_value), ('{:.' + str(-max_figures) + 'f}').format(r_error)))
    else:
        # without trailing .0
        return((str(int(r_value)), str(int(r_error))))

# More convenient extraction of value and error from ufloats
def noms(*args):
    if len(args) == 1:
        args = args[0]
    return nominal_values(args)

vals = noms

def stds(*args):
    if len(args) == 1:
        args = args[0]
    return std_devs(args)

def linregress(x, y):
    sqrt = math.sqrt

    x = vals(x)
    y = vals(y)

    N = len(y)
    Delta = N * sum(x**2) - (sum(x))**2

    A = (N * sum(x * y) - sum(x) * sum(y)) / Delta
    B = (sum(x**2) * sum(y) - sum(x) * sum(x * y)) / Delta

    sigma_y = sqrt(sum((y - A * x - B)**2) / (N - 2))

    A_error = sigma_y * math.sqrt(N / Delta)
    B_error = sigma_y * math.sqrt(sum(x**2) / Delta)

    return ufloat(A, A_error), ufloat(B, B_error)

def curve_fit(f, x, y, **kwargs):
    popt, pcov = scipy.optimize.curve_fit(f, vals(x), vals(y), **kwargs)
    if len(numpy.shape(pcov)) == 0 or numpy.any(numpy.isinf(pcov)):
        print("popt =", popt)
        print("pcov =", pcov)
        raise Exception("Fit unsuccessful!")
    return uncertainties.correlated_values(popt, pcov)

def ucurve_fit(f, x, y, **kwargs):
    return curve_fit(f, vals(x), vals(y), sigma=stds(y), **kwargs)

def mean(values, axis=0):
    return uncertainties.unumpy.uarray(numpy.mean(vals(values), axis=axis), scipy.stats.sem(vals(values), axis=axis))

# doesn't seem to work, see http://en.wikipedia.org/wiki/Weighted_mean#Dealing_with_variance
def umean(values, axis=0):
    if numpy.shape(values)[axis] == 1:
        return values
    w = numpy.sum(1 / stds(values)**2, axis=axis)
    m = numpy.sum(vals(values) / stds(values)**2, axis=axis) / w
    sigma = numpy.sqrt(1 / w / (len(values) - 1) * numpy.sum((vals(values) - m)**2 / stds(values)**2))
    return uncertainties.unumpy.uarray(m, sigma)

def cumean(values):
    C = numpy.array(uncertainties.covariance_matrix(values))
    Cinv = numpy.linalg.inv(C)
    W = numpy.repeat(1, len(C))
    sigma2 = 1 / (numpy.dot(W.T, numpy.dot(Cinv, W)))
    mu = sigma2 * numpy.dot(W.T, numpy.dot(Cinv, noms(values)))
    return uncertainties.ufloat(mu, numpy.sqrt(sigma2))

def constant(*name):
    if len(name) == 1:
        c = scipy.constants.physical_constants[name[0]]
        return ufloat(c[0], c[2])
    else:
        return [constant(n) for n in name]
