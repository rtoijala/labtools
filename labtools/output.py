#!/usr/bin/env python3

import pickle

def write_result(name, tex):
    f = open('build/result/' + name, 'w')
    f.write(tex + r'\endinput')
    f.close()

def write_table(name, tex):
    f = open('build/table/' + name, 'w')
    f.write(tex)
    f.close


def load_pickle(filename):
    ret = None
    with open(filename, 'rb') as f:
        ret = pickle.load(f)
    return ret

def dump_pickle(var, filename):
    with open(filename, 'wb') as f:
        pickle.dump(var, f)
